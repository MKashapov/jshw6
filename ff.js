function createNewUser (){
  let firstName = prompt('What is your name?');
  let lastName = prompt('What is your last name?');
  let birth = prompt( 'Enter your birthday date(text format:dd.mm.yyyy)');
  const newUser = {
      firstName: firstName,
      lastName: lastName,
      birth: birth,
      getLogin (firstName,lastName) {
          return (this.firstName.slice(0,1).toUpperCase() + this.lastName.toLowerCase() )
      },
      getAge (){
          let dateString = this.birth.split('.');
          let birthday = new Date(dateString[2],+dateString[1] - 1, dateString[0]);
          let nowDate = new Date();
          let age = nowDate.getFullYear() - birthday.getFullYear();
          let month = nowDate.getMonth() - birthday.getMonth();
          if (month < 0 || (month === 0 && nowDate.getDate() < birthday.getDate())){
              age = age - 1;
          }
          return age;
      },
      getPassword(){
          return(this.firstName.slice(0,1).toUpperCase() + this.lastName.toLowerCase()+ birth.slice(6,10))
      }
  }
  console.log()
  console.log(newUser.getPassword(),newUser.getAge(),newUser.getLogin())
}
createNewUser()
